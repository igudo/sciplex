import urllib.parse
from django.urls import reverse
from django.template import Context, Template, RequestContext
from django.template.loader import render_to_string, get_template
from django.core.mail import send_mail
from django.core.mail import EmailMessage
from random import choice
import string


def email_send(subject=None, body=None, to=None, reason=None):
    if type(to) == str:
        to = [to]
    EmailMessage(subject=subject, body=body, to=to).send()


def message(response, message, type=2):
    """
    :param response: Ответ сервера без сообщения
    :param message: Сообщение
    :param type: Тип сообщения: 1 - успешно, 2 - информация, 3 - предупреждение, 4 - ошибка
    :return: Ответ сервера с сообщением, хранящимся в cookie
    """
    response.set_cookie('emessage', urllib.parse.quote((str(type)+message)))
    return response


def allow_origins(response):
    response["Access-Control-Allow-Origin"] = "*"
    return response


def get_activation_code():
    activation_code = ''.join([choice(string.ascii_lowercase + string.ascii_uppercase + string.digits) for _ in range(50)])
    return activation_code
