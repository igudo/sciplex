from django.core import mail
from django.template.loader import render_to_string
from django.utils.html import strip_tags
from django.conf import settings
from profiles.models import UserAccount, UserReasons
from django.core.exceptions import ObjectDoesNotExist


class EmailLetterReason:
    """Причина создания письма - регистрация, тех. поддержка и тд"""
    def __init__(self, pk, reason_data):
        self.pk = pk
        self.template = reason_data[0]
        self.subject = reason_data[1]
        self.customizable = reason_data[2]
        self.verbose_name = reason_data[3]

    def get_contents(self, data):
        """Возвращает письмо в конечном виде (html и text), сгенерированное на основе всех предоставленных данных"""
        data['PARENT_HOST'] = settings.PARENT_HOST

        html_content = render_to_string('email/' + self.template, data)
        text_content = strip_tags(html_content)
        return html_content, text_content


class EmailLetter:
    """Письмо"""
    reasons = {
        # словарь причин писем. ключ = айди действия, значение = (
        #   темплейт письма, заголовок, вывод в настройках, название в настройках
        # )
        'registration': (
            'profiles/registration.html',
            'Добро пожаловать!',
            False,
            'Регистрация',
        ),
    }

    def send(self):
        html_content, text_content = self.reason.get_contents(self.data)
        mail.send_mail(self.reason.subject, text_content, settings.DEFAULT_FROM_EMAIL, self.to, html_message=html_content)

    def __init__(self, to, reason, subject=None, body=None, data=None):
        # проверка на то, что такая причина существует
        rs = self.reasons.get(reason, None)
        if rs is None:
            raise ValueError("This reason was not found")
        self.reason = EmailLetterReason(reason, rs)

        # Заголовок и тело письма должны быть либо None, либо str. Но пока что заголовок и тело,
        # переданные в класс, ни на что не влияют - всё используется от причины
        if subject is None and body is None:
            pass
        elif type(subject) != str or type(body) != str:
            raise ValueError("Data error")
        else:
            self.subject = subject
            self.body = body

        # словарь с данными должен быть dict
        if data is not None:
            self.data = data
        else:
            self.data = dict()

        # В to можно послать как email типа str, так и пользователя - в конце концов
        # это будет список майлов
        if type(to) == str:
            self.to = [to]
        elif to is not None:
            self.to = [to.university_email]
        elif type(to) == list:
            self.to = to
        else:
            raise ValueError("'to' must contain a string, a list, or user")
        self.to = [item for item in self.to if self.check_access(item, self.reason)]

    def check_access(self, email, reason):
        """Возвращает значение из БД. Значение булево, если истино - значит юзер разрешил
        отправку уведомлений по этой причине, если нет - значит запретил"""
        try:
            user = UserAccount.objects.get(university_email=email)
        except ObjectDoesNotExist:
            return False

        user_reason, created = UserReasons.objects.get_or_create(
            user=user,
            reason=reason.pk,
        )
        return user_reason.notifiable

