from django.contrib import admin

from .models import Spot, Meeting

admin.site.register(Meeting)
admin.site.register(Spot)
