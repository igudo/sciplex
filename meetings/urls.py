from django.urls import path
from . import views


urlpatterns = [
    path('mentors/', views.MentorsView.as_view(), name='mentors'),
]
