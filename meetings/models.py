from django.db import models
from django.contrib.auth import get_user_model

User = get_user_model()


class Spot(models.Model):
    lat = models.FloatField("широта")
    lon = models.FloatField("долгота")
    readable_address = models.CharField(max_length=300, verbose_name="адрес")
    description = models.TextField(null=True, blank=True, verbose_name="описание места")
    photo = models.ImageField(null=True, blank=True, verbose_name="картинка места")


class Meeting(models.Model):
    mentor_id = models.ForeignKey(
        User,
        on_delete=models.CASCADE,
        verbose_name="профиль наставника",
        related_name="mentor_id",
    )
    learner_id = models.ForeignKey(
        User,
        on_delete=models.CASCADE,
        verbose_name="профиль обучающегося",
        related_name="learner_id",
    )
    subject = models.CharField(max_length=100, verbose_name="предмет")
    description = models.TextField(verbose_name="описание задачи")
    spot_id = models.ForeignKey(Spot, on_delete=models.CASCADE, verbose_name="место")
    time = models.DateTimeField(verbose_name="время")
    is_approved = models.BooleanField(verbose_name="встреча согласована")
