from django import forms
from django.contrib.auth import (
    authenticate, get_user_model, password_validation,
)
from django.core.exceptions import ValidationError
from django.conf import settings
import requests
from django.contrib.admin.widgets import AdminDateWidget

User = get_user_model()


class MentorsForm(forms.Form):
    faculty = forms.ChoiceField(label="Факультет", choices=[])
    department = forms.ChoiceField(label="Кафедра", choices=[])
    year_of_university = forms.ChoiceField(label="Курс", choices=[])
    competence = forms.ChoiceField(label="Предмет", choices=[])

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        for visible in self.visible_fields():
            visible.field.widget.attrs['class'] = 'form-control'

        self.fields['faculty'].choices = [
            ('', 'Все'),
            ('its', 'ИТС'),
            ('fem', 'ФЭМ'),
            ('mto', 'МТО'),
        ]

        self.fields['department'].choices = [
            ('', 'Все'),
            ('metall', 'Металловедение'),
            ('osnovi_konstr_machines', 'Основы конструирования машин'),
            ('systems_of_privods', 'Системы приводов'),
            ('inf_systems', 'Информационные системы'),
            ('fin_management', 'Финансовый менеджмент'),
        ]

        self.fields['year_of_university'].choices = [
            ('', 'Все'),
            ('1', '1'),
            ('2', '2'),
            ('3', '3'),
            ('4', '4'),
        ]

        self.fields['competence'].choices = [
            ('', 'Все'),
            ('math_analise', 'Мат. анализ'),
            ('inf', 'Информатика'),
            ('lin_algebra', 'Лин. алгебра'),
            ('philosophy', 'Философия'),
        ]
        for field in self.fields.copy():
            self.fields[field].required = False
