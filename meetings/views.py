from django.shortcuts import render
from django.views import View
from django.contrib.auth import (
    authenticate, get_user_model, password_validation, login, logout
)
from django.http import HttpResponse, HttpResponseRedirect, JsonResponse
from .forms import MentorsForm
from django.urls import reverse
import tools
import tools.email
import datetime

User = get_user_model()


class MentorsView(View):
    form_class = MentorsForm
    template_name = 'meetings/mentors.html'

    def get(self, request, *args, **kwargs):
        if not request.user.is_authenticated:
            return HttpResponseRedirect('/')

        form = self.form_class(request.GET)
        data = {'form': form, 'title': 'Менторы'}

        faculty = request.GET.get('faculty', '')
        department = request.GET.get('department', '')
        year_of_university = request.GET.get('year_of_university', '')
        competence = request.GET.get('competence', '')

        mentors = User.objects.filter(show_in_mentors=True)

        if faculty != '':
            chs = form.fields['faculty'].choices
            faculty_val = None
            for ch in chs:
                if ch[0] == faculty:
                    faculty_val = ch[1]
                    break
            if faculty_val is not None:
                mentors = mentors.filter(faculty=faculty_val)

        if department != '':
            chs = form.fields['department'].choices
            val = None
            for ch in chs:
                if ch[0] == department:
                    val = ch[1]
                    break
            if val is not None:
                mentors = mentors.filter(department=val)

        if year_of_university != '':
            chs = form.fields['year_of_university'].choices
            val = None
            for ch in chs:
                if ch[0] == year_of_university:
                    val = ch[1]
                    break
            if val is not None:
                mentors = mentors.filter(year_of_university=val)

        if competence != '':
            chs = form.fields['competence'].choices
            val = None
            for ch in chs:
                if ch[0] == competence:
                    val = ch[1]
                    break
            if val is not None:
                mentors = mentors.filter(competencies=val)



        data['mentors'] = mentors

        return render(request, self.template_name, data)

