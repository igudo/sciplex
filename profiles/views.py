from django.shortcuts import render
from django.views import View
from django.contrib.auth import (
    authenticate, get_user_model, password_validation, login, logout
)
from django.http import HttpResponse, HttpResponseRedirect, JsonResponse
from .forms import UserRegistrationForm, LoginForm, MyProfileForm
from django.urls import reverse
import tools
from .models import UserActivation
import tools.email
import datetime
import random

User = get_user_model()


def gen_mentors(request):
    facultyes = [
        'ИТС',
        'ФЭМ',
        'МТО',
    ]

    departments = [
        'Металловедение',
        'Основы конструирования машин',
        'Системы приводов',
        'Информационные системы',
        'Финансовый менеджмент',
    ]

    year_of_university = [
        '1',
        '2',
        '3',
        '4',
    ]

    competencies = [
        'Мат. анализ',
        'Информатика',
        'Лин. алгебра',
        'Философия',
    ]

    fnames = [
        'Денис',
        'Никита',
        'Артем',
        'Михаил',
        'Руслан',
        'Кирилл',
        'Дмитрий',
        'Борис',
    ]

    lnames = [
        'Иванов',
        'Петров',
        'Бунин',
        'Пушкин',
        'Кот',
        'Гордый',
    ]

    patronymics = [
        'Иванович',
        'Петрович',
        'Кириллович',
        'Денисович',
        'Артемович',
    ]

    for i in range(150):
        User.objects.create(
            show_in_mentors=True,
            username=f'mentor{str(i)}',
            first_name=random.choice(fnames),
            last_name=random.choice(lnames),
            patronymic=random.choice(patronymics),
            faculty=random.choice(facultyes),
            department=random.choice(departments),
            year_of_university=random.choice(year_of_university),
            competencies=random.choice(competencies),
        ).set_password('123123123')
    return tools.message(HttpResponseRedirect('/'), message='Сгенерировано!', type=1)


class RegistrationView(View):
    form_class = UserRegistrationForm
    initial = {}
    template_name = 'profiles/signup.html'

    def get(self, request, *args, **kwargs):
        if request.user.is_authenticated:
            return HttpResponseRedirect('/')
        form = self.form_class(initial=self.initial)
        data = {'form': form, 'title': 'Регистрация'}
        return render(request, self.template_name, data)

    def post(self, request, *args, **kwargs):
        if request.user.is_authenticated:
            return HttpResponseRedirect('/')
        form = self.form_class(request.POST, request=request)
        if form.is_valid():
            new_user = form.save()
            new_user.is_active = False
            new_user.save()

            code = tools.get_activation_code()
            email_activation = UserActivation.objects.create(email=new_user.university_email, activation_code=code)
            tools.email.EmailLetter(new_user, 'registration', data={'user': new_user,
                                                                    'email_activation': email_activation}).send()
            return tools.message(HttpResponseRedirect('/'), message='Вам отправлено письмо для подтверждения электронной почты', type=1)

        return render(request, self.template_name, {'form': form})


class LogoutView(View):

    def get(self, request, *args, **kwargs):
        if not request.user.is_authenticated:
            return HttpResponseRedirect('/')
        logout(request)
        return HttpResponseRedirect(reverse('login'))


class LoginView(View):
    form_class = LoginForm
    initial = {}
    template_name = 'profiles/login.html'

    def get(self, request, *args, **kwargs):
        if request.user.is_authenticated:
            return HttpResponseRedirect('/')
        form = self.form_class(initial=self.initial)
        data = {'form': form}
        data['title'] = 'Авторизация'
        return render(request, self.template_name, data)

    def post(self, request, *args, **kwargs):
        if request.user.is_authenticated:
            return HttpResponseRedirect('/')
        form = self.form_class(request.POST)

        if form.is_valid():
            user = form.user_cache
            if not user.is_active:
                try:
                    email_item = UserActivation.objects.get(email=user.email)
                except UserActivation.DoesNotExist:
                    email_item = False

                if email_item:
                    if email_item.activation_code_date + datetime.timedelta(days=1) < datetime.date.today():
                        tools.email.EmailLetter(user, 'registration',
                                                data={'user': user,
                                                      'email_activation': email_item}).send()

                return tools.message(HttpResponseRedirect('/'), message='Для авторизации подтвердите почту, мы отправили Вам письмо с ссылкой для этого', type=3)
            login(request, user)
            return HttpResponseRedirect('/')

        return render(request, self.template_name, {'form': form})


class ActivationView(View):

    def get(self, request, activation_code, *args, **kwargs):
        try:
            code = UserActivation.objects.get(activation_code=activation_code)
        except:
            return tools.message(HttpResponseRedirect('/'), message='Кода активации не существует', type=3)
        email = code.email
        try:
            if code.activation_code_date + datetime.timedelta(days=20) < datetime.date.today():
                code = tools.get_activation_code()
                new_email_item = UserActivation.objects.create(email=email, activation_code=code)
                new_user = User.objects.get(university_email=email)
                tools.email.EmailLetter(new_user, 'registration', data={'user': new_user,
                                                                        'email_activation': new_email_item}).send()
                code.delete()
                return tools.message(HttpResponseRedirect('/'), message='Истекло время ожидания активации почты, отправлено новое письмо', type=3)
            else:
                user = User.objects.get(university_email=email)
                user.is_active = True
                user.save()
                code.delete()
                login(request, user)
                return tools.message(HttpResponseRedirect('/'), message='Почта подтверждена', type=1)
        except Exception as e:
            return tools.message(HttpResponseRedirect('/'), message='Кода активации не существует', type=3)


class MyProfileView(View):
    form_class = MyProfileForm
    initial = {}
    template_name = 'profiles/myprofile.html'

    def get(self, request, *args, **kwargs):
        if not request.user.is_authenticated:
            return HttpResponseRedirect('/')
        form = self.form_class(instance=request.user)
        data = {'form': form, 'title': 'Личный кабинет'}
        return render(request, self.template_name, data)

    def post(self, request, *args, **kwargs):
        if not request.user.is_authenticated:
            return HttpResponseRedirect('/')
        form = self.form_class(request.POST, instance=request.user)
        if form.is_valid():
            form.save()
            return tools.message(HttpResponseRedirect(reverse('myprofile')), message='Успешно', type=1)

        return render(request, self.template_name, {'form': form})
