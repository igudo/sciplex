from django.urls import path
from . import views


urlpatterns = [
    path('signup/', views.RegistrationView.as_view(), name='signup'),
    path('login/', views.LoginView.as_view(), name='login'),
    path('logout/', views.LogoutView.as_view(), name='logout'),
    path('activation/<str:activation_code>/', views.ActivationView.as_view(), name='activation'),
    path('myprofile/', views.MyProfileView.as_view(), name='myprofile'),
    path('gen_mentors/', views.gen_mentors, name='gen_mentors')
]
