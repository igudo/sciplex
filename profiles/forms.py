from django import forms
from django.contrib.auth import (
    authenticate, get_user_model, password_validation,
)
from django.core.exceptions import ValidationError
from django.conf import settings
import requests
from django.contrib.admin.widgets import AdminDateWidget
from . import validators

User = get_user_model()


class MyProfileForm(forms.ModelForm):
    class Meta:
        model = User
        error_css_class = 'error'
        fields = (
            'show_in_mentors',
        )
        labels = {
            'show_in_mentors': 'Показывать меня на странице с менторами',
        }

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        for visible in self.visible_fields():
            visible.field.widget.attrs['class'] = 'form-control'

        self.fields['show_in_mentors'].widget.attrs['class'] = 'form-check-input'

        non_required_fields = (
            'show_in_mentors',
        )

        for field in self.fields.copy():
            if field not in non_required_fields:
                self.fields[field].required = True
            else:
                self.fields[field].required = False


class UserRegistrationForm(forms.ModelForm):
    password1 = forms.CharField(max_length=50, widget=forms.PasswordInput, label="Пароль", validators=[validators.validate_password1])
    password2 = forms.CharField(max_length=50, widget=forms.PasswordInput, label="Подтверждение пароля", validators=[validators.validate_password2])

    class Meta:
        model = User
        error_css_class = 'error'
        fields = (
            'first_name',
            'last_name',
            'patronymic',
            'date_birth',
            'year_of_university',
            'faculty',
            'department',
            'competencies',
            'description',
            'contact_details',
            'email',
            'university_email',
            'username',
        )

        labels = {
            'first_name': 'Имя',
            'last_name': 'Фамилия',
            'description': 'Описание (необязательно)',
            'email': 'Дополнительная почта (необязательно)',
            'username': 'Логин',
        }

        widgets = {
            'date_birth': forms.DateInput(attrs={'type': 'date'}),
        }

    def __init__(self, *args, **kwargs):
        try:
            self.request = kwargs.pop('request')
        except:
            pass
        super().__init__(*args, **kwargs)
        for visible in self.visible_fields():
            visible.field.widget.attrs['class'] = 'form-control'

        non_required_fields = (
            'description',
            'email',
        )

        for field in self.fields.copy():
            if field not in non_required_fields:
                self.fields[field].required = True
            else:
                self.fields[field].required = False

    def clean_password2(self):
        password1 = self.cleaned_data.get("password1")
        password2 = self.cleaned_data.get("password2")
        if password1 and password2 and password1 != password2:
            raise forms.ValidationError(
                'Пароли не совпадают'
            )
        return password2

    def clean_username(self):
        if User.objects.filter(username=self.cleaned_data.get('username')).exists():
            raise forms.ValidationError('Такой пользователь уже существует.')
        return self.cleaned_data.get('username')

    def clean_university_email(self):
        if User.objects.filter(university_email=self.cleaned_data.get('university_email')).exists():
            raise forms.ValidationError('Пользователь с таким email уже существует.')
        return self.cleaned_data.get('university_email')

    def clean(self):
        # если капча настроена и указана в настройках, то проверяем правильность
        try:
            if settings.RECAPTCHA_SECRET_KEY:
                flag = True
        except AttributeError:
            flag = False
        if flag:
            response = requests.post('https://www.google.com/recaptcha/api/siteverify', {
                'secret': settings.RECAPTCHA_SECRET_KEY,
                'response': self.request.POST['g-recaptcha-response']
            })
            if not response.json()['success']:
                raise ValidationError('Подтвердите, что вы не робот')
        return self.cleaned_data

    def save(self, commit=True):
        user = super().save(commit=False)
        user.set_password(self.cleaned_data["password1"])
        if commit:
            user.save()
        return user


class LoginForm(forms.Form):

    class Meta:
        error_css_class = 'error'

    login = forms.CharField(max_length=60, label='Имя пользователя или Email')
    password = forms.CharField(max_length=50, widget=forms.PasswordInput, label='Пароль')

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        for visible in self.visible_fields():
            visible.field.widget.attrs['class'] = 'form-control mb-3'
            visible.field.widget.attrs['required'] = 'required'
        self.user_cache = None

    def clean(self):
        password = self.cleaned_data.get('password', None)
        login = self.cleaned_data.get('login', None)
        try:
            user = User.objects.get(username=login)
        except User.DoesNotExist:
            try:
                user = User.objects.get(email=login)
            except User.DoesNotExist:
                user = None
        if user is None:
            raise ValidationError('Неверный логин или пароль')

        if not user.check_password(password):
            raise ValidationError('Неверный логин или пароль')

        self.user_cache = user

        return self.cleaned_data

