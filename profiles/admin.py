from django.contrib import admin
from .models import *
from django.contrib.auth.admin import UserAdmin
from django.conf import settings


class UserAccountAdmin(UserAdmin):
    fieldsets = UserAdmin.fieldsets + (
        ('Доп. поля', {'fields': (
            'patronymic',
            'date_birth',
            'year_of_university',
            'faculty',
            'department',
            'competencies',
            'description',
            'image',
            'contact_details',
            'university_email',
            'show_in_mentors',
        )}),
    )


admin.site.register(UserAccount, UserAccountAdmin)
admin.site.register(SocialNetwork)
admin.site.register(Competences)
