import requests
from django.contrib.auth import get_user_model
from django.db import models
from django.contrib.auth.models import AbstractUser
import datetime


def generate_url(instance, filename):
    """Generate url for user avatar"""
    file_type = filename.split('.')[-1]  # take file type (png, jpg, ..)
    last_id = UserAccount.objects.all().order_by('id')  # find last user
    if last_id:  # if exists
        last_id = last_id[0].id  # take his id
    if not last_id:  # else
        new_id = 1  # will be 1
    else:  # if exists
        new_id = last_id + 1  # plus 1
    filename = 'image-' + str(new_id) + '.' + file_type  # compare all
    return "avatars/" + filename  # returning url


class UserAccount(AbstractUser):
    """User account with additional fields"""
    patronymic = models.CharField(verbose_name='Отчество', max_length=255, blank=True, null=True)
    date_birth = models.DateField(verbose_name='Дата рождения', blank=True, null=True)
    year_of_university = models.IntegerField(verbose_name='Курс', blank=True, null=True)
    faculty = models.CharField(verbose_name='Факультет', max_length=255, blank=True, null=True)
    department = models.CharField(verbose_name='Кафедра', max_length=255, blank=True, null=True)
    competencies = models.CharField(verbose_name='Компетенции', max_length=255, blank=True, null=True)
    description = models.CharField(verbose_name='Описание', max_length=255, blank=True, null=True)
    image = models.ImageField(upload_to=generate_url, default='default/user.png', blank=False, null=False, verbose_name="Аватарка")
    contact_details = models.CharField(verbose_name='Контактные данные', max_length=255, blank=True, null=True)
    university_email = models.EmailField(verbose_name='Почта университета', unique=True, max_length=255, null=True)
    rating = models.FloatField(verbose_name="Рейтинг", default=0)
    coins = models.IntegerField(verbose_name="Количество монет", default=0)
    show_in_mentors = models.BooleanField(default=False, verbose_name='Показывать на странице с менторами')

    def __str__(self):
        return str(self.username)

    class Meta:
        verbose_name = 'Аккаунт пользователя'
        verbose_name_plural = 'Аккаунты пользователей'


User = get_user_model()


class SocialNetwork(models.Model):
    user_id = models.ForeignKey(User, on_delete=models.CASCADE)
    social_network = models.CharField(
        max_length=30, verbose_name="название социальной сети"
    )
    # Link should start with http:// or https://
    link = models.TextField(verbose_name="ссылка")

    def save(self, *args, **kwargs):
        """
        links validation
        """
        if not (
            str(self.link).startswith("http://")
            or str(self.link).startswith("https://")
        ):
            raise ValueError("Link is invalid, should start from http:// or https://")

        try:
            status_code = requests.get(self.link).status_code
        except Exception as e:
            raise ValueError("Link is invalid, error")

        if status_code != 200:
            raise ValueError(f"Link is invalid, status code is {status_code}")

        super(SocialNetwork, self).save(*args, **kwargs)

    class Meta:
        verbose_name = "социальная сеть"
        verbose_name_plural = "социальные сети"


class Competences(models.Model):
    user_id = models.ForeignKey(
        User, on_delete=models.CASCADE, verbose_name="пользователь"
    )
    # Хранятся через запятую
    competences = models.TextField(verbose_name="компетенции")
    subjects = models.TextField(verbose_name="предметы")

    class Meta:
        verbose_name = "компетенция"
        verbose_name_plural = "компетенции"


class UserReasons(models.Model):
    user = models.ForeignKey('UserAccount', on_delete=models.CASCADE)
    reason = models.CharField(max_length=256)
    notifiable = models.BooleanField(default=True)


class UserActivation(models.Model):
    email = models.CharField(verbose_name='Электронная почта', max_length=128, blank=False)
    activation_code = models.CharField(verbose_name='Код активации почты', max_length=128, blank=False, null=False)
    activation_code_date = models.DateField(verbose_name='Дата создания кода активации', default=datetime.date.today,
                                            max_length=128, blank=False)

    def __str__(self):
        return str(self.email)
